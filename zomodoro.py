from app.libraries.engine import Engine
from app.libraries.drawer import Drawer
from app.libraries.mixer import Mixer
from app.libraries.bus import Bus
from app.zomodoro import Zomodoro

width, height = 250, 100
window_title = 'Zomodoro'
engine = Engine(width, height, window_title)

if __name__ == '__main__':
  Zomodoro(Bus(), engine, Drawer(), Mixer())
