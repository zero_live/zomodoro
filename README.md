# Zomodoro

This is a personal project for learning about pygame library and how to tests game behaviour. And this way I have my own Pomodoro Application ;)

## System requirements

- `Docker version 17.12.1-ce` or higher.
- `docker-compose version 1.18.0` or higher.

## Run tests:

For run all the tests you have to run the following commands in the project folder:

- `docker-compose up --build`
- `docker-compose run --rm app python -m unittest discover tests -v`

## For run the app:

### System requirements:

- `Python 2.7.14` or higher.

### Run the app:

For run the app you have to run the following command in the project folder:

- `python -m zomodoro`

## Project Contributions

This project is at public [repository at gitlab](https://gitlab.com/zero_live/zomodoro) where you can do your pull requests, it be reviewed after pass the [gitlab CI](https://gitlab.com/zero_live/zomodoro/pipelines)

## Organization

I have a Trello for organize this project with its Ideas and things to do. You can visit it clicking [here](https://trello.com/b/JPjOS8ic/zomodoro).
