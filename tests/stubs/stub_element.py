
class StubElement():
  def __init__(self, bus):
    self.__bus = bus
    self.__payload = None

  def subscribe(self, topic):
    self.__bus.subscribe(topic, (lambda payload: self.__event(payload)))

  def retrieved_payload(self):
    return self.__payload

  def __event(self, payload):
    self.__payload = payload
