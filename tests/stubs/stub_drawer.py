
class StubDrawer():
  def __init__(self):
    self.__text = []
    self.__timer = ''
    self.__background_drawed = False

  def text(self, text, size, position, has_update = False):
    if not has_update: self.__text.append(text)
    if has_update: self.__timer = text

  def background(self):
    self.__background_drawed = True

  def is_background_drawed(self):
    return self.__background_drawed

  def drawed_text(self):
    return self.__text

  def drawed_timer(self):
    return self.__timer
