
class StubEngine():
  def __init__(self):
    self.__events = []

  def add_events(self, events):
    self.__events = events

  def events(self):
    event = self.__events.pop(0)
    return [event]
