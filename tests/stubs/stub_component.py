
class StubComponent():
  def __init__(self, bus):
    self.__bus = bus
    self.__was_called = False

  def subscribe(self, topic):
    self.__bus.subscribe(topic, (lambda _: self.__event()))

  def was_called(self):
    return self.__was_called

  def __event(self):
    self.__was_called = True
