
class StubMixer():
  def __init__(self):
    self.__power = False
    self.__alarm = False

  def play_power(self):
    self.__power = True

  def play_alarm(self):
    self.__alarm = True

  def was_power_play(self):
    return self.__power

  def was_alarm_play(self):
    return self.__alarm
