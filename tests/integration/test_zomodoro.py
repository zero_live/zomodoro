from tests.stubs.stub_engine import StubEngine
from tests.stubs.stub_drawer import StubDrawer
from tests.stubs.stub_mixer import StubMixer
from app.libraries.event import Event
from app.libraries.bus import Bus
from app.zomodoro import Zomodoro
import unittest

class TestZomodoro(unittest.TestCase):
  def test_zomodoro_can_be_closed(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events([Event('close_app', None)])
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.__assert_app_is_closed()

  def test_zomodoro_starts_when_press_space_key(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '05:00')

  def test_zomodoro_can_be_paused(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_pause())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '05:00')

  def test_zomodoro_can_be_resumed(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_resumed())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '04:59')

  def test_zomodoro_stops_the_count_down_when_reach_zero(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_try_pass_zero())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '00:00')

  def test_zomodoro_plays_a_sound_every_time_power_key_is_pressed(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events([Event('power_button', None), Event('close_app', None)])
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(mixer.was_power_play(), True)

  def test_zomodoro_plays_a_sound_when_count_down_is_reached(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events([Event('count_down_reached', None), Event('close_app', None)])
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(mixer.was_alarm_play(), True)

  def test_zomodoro_starts_short_break_after_first_pomodoro(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_first_break())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '05:00')

  def test_zomodoro_starts_second_pomodoro_after_first_break(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_second_pomodoro())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '25:00')

  def test_zomodoro_starts_second_break_after_second_pomodoro(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_second_break())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '05:00')

  def test_zomodoro_starts_thrid_pomodoro_after_second_break(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_third_pomodoro())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '25:00')

  def test_zomodoro_starts_long_break_after_last_pomodoro(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_long_break())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '15:00')

  def test_zomodoro_shows_application_information(self):
    info = ['Space: Start/Pause     Alt+F4: Close', 'Click: Start/Pause Backscpace: Reset']
    bus = Bus()
    engine = StubEngine()
    engine.add_events([Event('tick', { 'moment': 0 }), Event('close_app', None)])
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_text(), info)

  def test_zomodoro_can_reset_actual_count_down(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_start_and_reset_pomodoro())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '25:00')

  def test_zomodoro_can_reset_count_downs(self):
    bus = Bus()
    engine = StubEngine()
    engine.add_events(self.__events_for_move_to_first_break_and_reset_count_downs())
    drawer = StubDrawer()
    mixer = StubMixer()

    Zomodoro(bus, engine, drawer, mixer)

    self.assertEqual(drawer.drawed_timer(), '25:00')

  def __assert_app_is_closed(self):
    self.assertEqual(True, True)

  def __events_for_start_long_break(self):
    pomodoro = 1500000
    short_break = 300000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_start_third_pomodoro(self):
    pomodoro = 1500000
    short_break = 300000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_start_second_break(self):
    pomodoro = 1500000
    short_break = 300000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_start_second_pomodoro(self):
    pomodoro = 1500000
    short_break = 300000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': short_break }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_start_first_break(self):
    pomodoro = 1500000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_try_pass_zero(self):
    time_for_reach_zero = 1500000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': time_for_reach_zero }),
      Event('close_app', None)
    ]

  def __events_for_resumed(self):
    return [
      Event('power_button', None),
      Event('tick', { 'moment': 1200000 }),
      Event('power_button', None),
      Event('power_button', None),
      Event('tick', { 'moment': 1201000 }),
      Event('close_app', None)
    ]

  def __events_for_pause(self):
    return [
      Event('power_button', None),
      Event('tick', { 'moment': 1200000 }),
      Event('power_button', None),
      Event('tick', { 'moment': 1201000 }),
      Event('close_app', None)
    ]

  def __events_for_start(self):
    return [
      Event('tick', { 'moment': 1200000 }),
      Event('power_button', None),
      Event('tick', { 'moment': 2400000 }),
      Event('close_app', None)
    ]

  def __events_for_start_and_reset_pomodoro(self):
    pomodoro = 1500000
    short_break = 300000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('reset_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

  def __events_for_move_to_first_break_and_reset_count_downs(self):
    pomodoro = 1500000

    return [
      Event('power_button', None),
      Event('tick', { 'moment': pomodoro }),
      Event('tick', { 'moment': 0 }),
      Event('power_button', None),
      Event('tick', { 'moment': 0 }),
      Event('reset_button', None),
      Event('reset_button', None),
      Event('tick', { 'moment': 0 }),
      Event('close_app', None)
    ]

if __name__ == '__main__':
  unittest.main()
