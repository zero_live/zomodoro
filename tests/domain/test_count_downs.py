from app.domain.count_downs import CountDowns
import unittest

class TestCountDowns(unittest.TestCase):
  def test_count_downs_retrieves_first_pomodoro(self):
    pomodoro = 1500000
    count_downs = CountDowns()

    count_down = count_downs.retrieve()

    self.assertEqual(count_down.time_left(0), pomodoro)

  def test_count_downs_retrieves_first_break_after_first_pomodoro(self):
    first_break = 300000
    count_downs = CountDowns()
    count_downs.retrieve()

    count_down = count_downs.retrieve()

    self.assertEqual(count_down.time_left(0), first_break)

  def test_count_downs_retrieves_first_pomodofo_after_long_break(self):
    pomodoro = 1500000
    count_downs = CountDowns()
    self.__move_to_long_break(count_downs)

    count_down = count_downs.retrieve()

    self.assertEqual(count_down.time_left(0), pomodoro)

  def test_count_downs_can_reset_acutal_count_down(self):
    pomodoro = 1500000
    time_after = 100000
    time_more_later = time_after + time_after
    count_downs = CountDowns()
    count_down = count_downs.retrieve()
    count_down.power()
    count_down.time_left(time_after)

    count_down = count_downs.reset()

    self.assertEqual(count_down.time_left(time_more_later), pomodoro)

  def test_count_downs_can_be_reset(self):
    pomodoro = 1500000
    time_after = 100000
    time_more_later = time_after + time_after
    count_downs = CountDowns()
    count_downs.retrieve()
    count_down = count_downs.retrieve()
    count_down.power()
    count_down.time_left(time_after)

    count_downs.reset()
    count_down = count_downs.reset()

    self.assertEqual(count_down.time_left(time_more_later), pomodoro)

  def __move_to_long_break(self, count_downs):
    long_break = 6

    for _ in range(long_break):
      count_downs.retrieve()

if __name__ == '__main__':
  unittest.main()
