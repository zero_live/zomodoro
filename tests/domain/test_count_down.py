from app.domain.count_down import CountDown
import unittest

class TestCountDown(unittest.TestCase):
  def test_count_down_knows_if_never_was_started(self):
    any_time = 1000

    count_down = CountDown(any_time)

    self.assertEqual(count_down.was_started(), False)

  def test_count_down_knows_if_was_started(self):
    any_time = 1000
    count_down = CountDown(any_time)

    count_down.power()

    self.assertEqual(count_down.was_started(), True)

if __name__ == '__main__':
  unittest.main()
