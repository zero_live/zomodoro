from tests.stubs.stub_component import StubComponent
from app.libraries.bus import Bus
import unittest

class TestBus(unittest.TestCase):
  def test_bus_calls_callbacks_subcribed_to_a_topic(self):
    topic = 'topic'
    bus = Bus()
    component = StubComponent(bus)
    component.subscribe(topic)

    bus.publish(topic, None)

    self.assertEqual(component.was_called(), True)

  def __called(self):
    self.called = True

if __name__ == '__main__':
  unittest.main()
