from app.libraries.timer import Timer
import unittest

class TestTimer(unittest.TestCase):
  def test_timer_translate_millisecons_to_minutes_and_seconds(self):
    milliseconds = 123456

    translated = Timer.translate(milliseconds)

    self.assertEqual(translated, '02:03')

if __name__ == '__main__':
  unittest.main()
