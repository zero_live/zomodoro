from tests.stubs.stub_app_event import StubAppEvent
from app.libraries.event import Event
import unittest

class TestEvent(unittest.TestCase):
  def test_event_builds_from_app_event(self):
    engine_close_code = 12
    app_event = StubAppEvent(engine_close_code)

    event = Event.build(app_event)

    self.assertEqual(event.topic(), 'close_app')

  def test_event_translated_enter_key_pressed_to_power_button(self):
    engine_key_pressed_code = 2
    engine_enter_key_code = 32
    app_event = StubAppEvent(engine_key_pressed_code, engine_enter_key_code)

    event = Event.build(app_event)

    self.assertEqual(event.topic(), 'power_button')

  def test_event_translated_enter_key_pressed_to_reset_button(self):
    engine_key_pressed_code = 2
    engine_backspace_key_code = 8
    app_event = StubAppEvent(engine_key_pressed_code, engine_backspace_key_code)

    event = Event.build(app_event)

    self.assertEqual(event.topic(), 'reset_button')

  def test_event_translated_mouse_key_pressed_to_power_button(self):
    engine_mouse_pressed_code = 5
    app_event = StubAppEvent(engine_mouse_pressed_code)

    event = Event.build(app_event)

    self.assertEqual(event.topic(), 'power_button')

if __name__ == '__main__':
  unittest.main()
