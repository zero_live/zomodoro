from tests.stubs.stub_element import StubElement
from app.components.chronometers import Chronometers
from app.libraries.bus import Bus
import unittest

class TestChronometers(unittest.TestCase):
  def test_chronometers_doesnt_start_by_it_self(self):
    five_minutes_left = 1200000
    pomodoro = '25:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    bus.publish('tick', { 'moment': five_minutes_left })

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_starts_when_it_receives_the_event(self):
      pomodoro = '05:00'
      bus = Bus()
      element = StubElement(bus)
      element.subscribe('draw_time_left')

      Chronometers(bus)
      bus.publish('power_button', None)
      bus.publish('tick', { 'moment': 1200000 })

      retrieved = element.retrieved_payload()
      self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_can_be_paused(self):
    pomodoro = '05:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1200000 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1201000 })

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_can_be_resumed(self):
    pomodoro = '04:59'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1200000 })
    bus.publish('power_button', None)
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1201000 })

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_stops_the_count_down_when_reach_zero(self):
    time_for_reach_zero = 1500000
    pomodoro = '00:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': time_for_reach_zero })

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_starts_first_break_after_first_pomdoro(self):
    short_break = '05:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    self.publications_for_start_first_break(bus)

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], short_break)

  def test_chronometers_starts_second_pomodoro_after_first_break(self):
    second_pomodoro = '25:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    self.publications_for_start_second_pomodoro(bus)

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], second_pomodoro)

  def test_chronometers_starts_second_break_after_second_pomodoro(self):
    second_break = '05:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    self.publications_for_start_second_break(bus)

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], second_break)

  def test_chronometers_starts_thrid_pomdoro_after_second_break(self):
    pomodoro = '25:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    self.publications_for_start_third_pomodoro(bus)

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def test_chronometers_starts_long_break_after_last_pomodoro(self):
    long_break = '15:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    self.publications_for_start_long_break(bus)

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], long_break)

  def test_chronometers_can_reset_actual_count_down(self):
    pomodoro = '25:00'
    bus = Bus()
    element = StubElement(bus)
    element.subscribe('draw_time_left')

    Chronometers(bus)
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1500000 })
    bus.publish('reset_button', None)
    bus.publish('tick', { 'moment': 0 })

    retrieved = element.retrieved_payload()
    self.assertEqual(retrieved['time_left'], pomodoro)

  def publications_for_start_first_break(self, bus):
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 1500000 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 0 })

  def publications_for_start_second_pomodoro(self, bus):
    pomodoro = 1500000
    short_break = 300000

    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })

  def publications_for_start_second_break(self, bus):
    pomodoro = 1500000
    short_break = 300000

    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 0 })

  def publications_for_start_third_pomodoro(self, bus):
    pomodoro = 1500000
    short_break = 300000

    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })

  def publications_for_start_long_break(self, bus):
    pomodoro = 1500000
    short_break = 300000

    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': short_break })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': pomodoro })
    bus.publish('tick', { 'moment': 0 })
    bus.publish('power_button', None)
    bus.publish('tick', { 'moment': 0 })

if __name__ == '__main__':
  unittest.main()
