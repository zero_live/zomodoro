from tests.stubs.stub_mixer import StubMixer
from app.elements.power import Power
from app.libraries.bus import Bus
import unittest

class TestPower(unittest.TestCase):
  def test_power_plays_a_sound(self):
    bus = Bus()
    mixer = StubMixer()

    Power(bus, mixer)
    bus.publish('power_button', None)

    self.assertEqual(mixer.was_power_play(), True)

if __name__ == '__main__':
  unittest.main()
