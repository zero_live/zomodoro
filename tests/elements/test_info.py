from tests.stubs.stub_drawer import StubDrawer
from app.elements.info import Info
from app.libraries.bus import Bus
import unittest

class TestInfor(unittest.TestCase):
  def test_info_draws_application_information(self):
    info = ['Space: Start/Pause     Alt+F4: Close', 'Click: Start/Pause Backscpace: Reset']
    bus = Bus()
    drawer = StubDrawer()

    Info(bus, drawer)
    bus.publish('tick', None)

    self.assertEqual(drawer.drawed_text(), info)

if __name__ == '__main__':
  unittest.main()
