from tests.stubs.stub_mixer import StubMixer
from app.elements.alarm import Alarm
from app.libraries.bus import Bus
import unittest

class TestAlarm(unittest.TestCase):
  def test_alarm_plays_a_sound_when_count_down_is_reached(self):
    bus = Bus()
    mixer = StubMixer()

    Alarm(bus, mixer)
    bus.publish('count_down_reached', None)

    self.assertEqual(mixer.was_alarm_play(), True)

if __name__ == '__main__':
  unittest.main()
