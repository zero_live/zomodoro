from tests.stubs.stub_drawer import StubDrawer
from app.elements.chronometer import Chronometer
from app.libraries.bus import Bus
import unittest

class TestChronometer(unittest.TestCase):
  def test_chronometer_draws_the_pomodoro(self):
    pomodoro = '25:00'
    bus = Bus()
    drawer = StubDrawer()

    chronometer = Chronometer(bus, drawer)
    bus.publish('draw_time_left', { 'time_left': pomodoro })

    self.assertEqual(drawer.drawed_timer(), pomodoro)

if __name__ == '__main__':
  unittest.main()
