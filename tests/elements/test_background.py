from tests.stubs.stub_drawer import StubDrawer
from app.elements.background import Background
from app.libraries.bus import Bus
import unittest

class TestBackground(unittest.TestCase):
  def test_background_draws_it_self_every_tick(self):
    bus = Bus()
    drawer = StubDrawer()

    Background(bus, drawer)
    bus.publish('tick', None)

    self.assertEqual(drawer.is_background_drawed(), True)

if __name__ == '__main__':
  unittest.main()
