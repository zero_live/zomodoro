from tests.stubs.stub_component import StubComponent
from app.services.event_service import EventService
from tests.stubs.stub_engine import StubEngine
from app.libraries.event import Event
from app.libraries.bus import Bus
import unittest

class TestEventService(unittest.TestCase):
  def test_event_service_publish_engine_events(self):
    engine_event = 'engine_event'
    bus = Bus()
    component = StubComponent(bus)
    component.subscribe(engine_event)
    engine = StubEngine()
    events = [Event(engine_event, None), Event('close_app', None)]
    engine.add_events(events)

    EventService(bus, engine)

    self.assertEqual(component.was_called(), True)

if __name__ == '__main__':
  unittest.main()
