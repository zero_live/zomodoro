from app.services.event_service import EventService

from app.components.chronometers import Chronometers

from app.elements.background import Background
from app.elements.alarm import Alarm
from app.elements.chronometer import Chronometer
from app.elements.power import Power
from app.elements.info import Info

class Zomodoro():
  def __init__(self, bus, engine, drawer, mixer):
    self.__elements = [
      Chronometer(bus, drawer),
      Background(bus, drawer),
      Power(bus, mixer),
      Alarm(bus, mixer),
      Info(bus, drawer)
    ]
    self.__components = [Chronometers(bus)]
    self.__services = [EventService(bus, engine)]
