from app.infrastructure.count_downs import ConfiguredCountDowns
from app.domain.count_down import CountDown

class CountDowns():
  __FIRST_POMODORO = 1
  __LONG_BREAK = 6

  def __init__(self):
    self.__current = 0

  def retrieve(self):
    self.__move_to_next_count_down()

    return self.__current_count_down()

  def reset(self):
    if not self.__count_down.was_started():
      self.__current = self.__FIRST_POMODORO

    return self.__current_count_down()

  def __current_count_down(self):
    count_down = ConfiguredCountDowns().select(self.__current)

    self.__count_down = CountDown(count_down)

    return self.__count_down

  def __move_to_next_count_down(self):
    self.__current += 1

    if self.__has_to_be_reset(): self.__reset()

  def __has_to_be_reset(self):
    return self.__current > self.__LONG_BREAK

  def __reset(self):
    self.__current = self.__FIRST_POMODORO
