
class CountDown():
  __END_TIME = 0

  def __init__(self, count_down):
    self.__was_started = False
    self.__current_time = count_down
    self.__paused_at = count_down
    self.__started = False
    self.__started_at = 0

  def power(self):
    self.__was_started = True
    self.__started = not self.__started

  def time_left(self, milliseconds):
    if not self.__started:
      return self.__paused_time(milliseconds)

    return self.__actual_time(milliseconds)

  def was_started(self):
    return self.__was_started

  def __paused_time(self, milliseconds):
    self.__started_at = milliseconds
    self.__paused_at = self.__current_time

    return self.__paused_at

  def __actual_time(self, milliseconds):
    if self.is_zero_reached(): return self.__END_TIME

    self.__current_time = self.__actual_time_from(milliseconds)

    return self.__current_time

  def __actual_time_from(self, milliseconds):
    return (self.__paused_at) - (milliseconds - self.__started_at)

  def is_zero_reached(self):
    return self.__current_time <= self.__END_TIME
