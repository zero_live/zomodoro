from app.domain.count_downs import CountDowns
from app.libraries.timer import Timer

class Chronometers():
  def __init__(self, bus):
    self.__bus = bus
    self.__count_downs = CountDowns()
    self.__set_count_down()
    self.__subscriptions()

  def __set_count_down(self):
    self.__count_down = self.__count_downs.retrieve()

  def __subscriptions(self):
    self.__bus.subscribe('tick', (lambda payload: self.__send_time(payload)))
    self.__bus.subscribe('power_button', (lambda payload: self.__power()))
    self.__bus.subscribe('reset_button', (lambda payload: self.__reset()))

  def __power(self):
    self.__count_down.power()

  def __reset(self):
    self.__count_down = self.__count_downs.reset()

  def __send_time(self, payload):
    if self.__count_down.is_zero_reached():
      self.__set_count_down()
      self.__bus.publish('count_down_reached', None)

    time_left = self.__count_down.time_left(payload['moment'])

    self.__send_draw(time_left)

  def __send_draw(self, time_left):
    data = { 'time_left': Timer.translate(time_left) }
    self.__bus.publish('draw_time_left', data)
