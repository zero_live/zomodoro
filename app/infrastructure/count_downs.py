import json
import os

class ConfiguredCountDowns():
  __PATH = 'app/config/count_downs.json'

  def __init__(self):
    count_downs_path = os.path.realpath(self.__PATH)

    with open(count_downs_path) as file:
      self.__count_downs = json.load(file)

  def select(self, current):
    return self.__count_downs[str(current)]
