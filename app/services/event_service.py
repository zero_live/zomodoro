
class EventService():
  def __init__(self, bus, engine):
    self.__bus = bus
    self.__engine = engine
    self.__running =  True
    self.__subscriptions()
    self.__retrieve_events()

  def __subscriptions(self):
    self.__bus.subscribe('close_app', (lambda _: self.__stop_service()))

  def __retrieve_events(self):
    while self.__running:
      for event in self.__engine.events():
        self.__bus.publish(event.topic(), event.payload())

  def __stop_service(self):
    self.__running = False
