
class Background():
  def __init__(self, bus, drawer):
    self.__bus = bus
    self.__drawer = drawer
    self.__subscriptions()

  def __subscriptions(self):
    self.__bus.subscribe('tick', (lambda _: self.__draw()))

  def __draw(self):
    self.__drawer.background()
