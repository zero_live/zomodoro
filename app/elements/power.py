
class Power():
  def __init__(self, bus, mixer):
    self.__bus = bus
    self.__mixer = mixer
    self.__subscriptions()

  def __subscriptions(self):
    self.__bus.subscribe('power_button', (lambda _: self.__play()))

  def __play(self):
    self.__mixer.play_power()
