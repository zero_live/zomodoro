
class Chronometer():
  __POSITION = [235, 20]
  __SIZE = 75

  def __init__(self, bus, drawer):
    self.__bus = bus
    self.__drawer = drawer
    self.__subscriptions()

  def __subscriptions(self):
    self.__bus.subscribe('draw_time_left', (lambda payload: self.__draw(payload)))

  def __draw(self, payload):
    text = payload['time_left']
    update_screen = True
    self.__drawer.text(text, self.__SIZE, self.__POSITION, update_screen)
