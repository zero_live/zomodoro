
class Info():
  __FIRST_LINE = 'Space: Start/Pause     Alt+F4: Close'
  __SECOND_LINE = 'Click: Start/Pause Backscpace: Reset'
  __SIZE = 10
  __FIRST_POSITION = [230, 2]
  __SECOND_POSITION = [230, 15]

  def __init__(self, bus, drawer):
    self.__bus = bus
    self.__drawer = drawer
    self.__subscriptions()

  def __subscriptions(self):
    self.__bus.subscribe('tick', (lambda _: self.__draw()))

  def __draw(self):
    self.__drawer.text(self.__FIRST_LINE, self.__SIZE, self.__FIRST_POSITION)
    self.__drawer.text(self.__SECOND_LINE, self.__SIZE, self.__SECOND_POSITION)
