from app.libraries.event import Event
import pygame

class Engine():
  def __init__(self, width, height, title):
    pygame.init()
    pygame.display.set_mode((width, height))
    pygame.display.set_caption(title)

  def events(self):
    events = [Event('tick', { 'moment': self.__moment() })]

    for event in pygame.event.get():
      event = Event.build(event)
      events.append(event)

    return events

  def __moment(self):
    return pygame.time.get_ticks()
