
class Timer():
  __SEPARATOR = ':'
  __DIGITS = 2

  @staticmethod
  def translate(milliseconds):
    return Timer.__calculate_time(milliseconds)

  @staticmethod
  def __calculate_time(milliseconds):
    minutes = str(Timer.__minutes_from(milliseconds))
    seconds = str(Timer.__seconds_from(milliseconds))

    return Timer.__text_with(minutes, seconds)

  @staticmethod
  def __text_with(minutes, seconds):
    return minutes.zfill(Timer.__DIGITS) + Timer.__SEPARATOR + seconds.zfill(Timer.__DIGITS)

  @staticmethod
  def __minutes_from(milliseconds):
    minutes = milliseconds / 60000
    return int(minutes)

  @staticmethod
  def __seconds_from(milliseconds):
    seconds = (milliseconds / 1000) % 60
    return int(seconds)
