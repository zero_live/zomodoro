
class Event():
  __POWER = 'power'
  __BUTTON = '_button'
  __KEYS = {
    32: __POWER,
    8: 'reset'
  }
  __CODES = {
    2: __BUTTON,
    5: __POWER + __BUTTON,
    12: 'close_app'
  }

  @staticmethod
  def build(engine_event):
    topic = Event.__build_topic(engine_event)
    payload = None
    return Event(topic, payload)

  def __init__(self, topic, payload):
    self.__topic = topic
    self.__payload = payload

  def topic(self):
    return self.__topic

  def payload(self):
    return self.__payload

  @staticmethod
  def __build_topic(event):
    try:
      topic = Event.__KEYS[event.key] + Event.__CODES[event.type]
    except:
      topic = Event.__CODES.get(event.type, event.type)

    return topic
