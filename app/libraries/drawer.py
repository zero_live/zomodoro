import pygame
import os

class Drawer():
  __FONT_PATH = os.path.realpath('app/resources/font/hack-bold.ttf')
  __ICON_PATH = os.path.realpath('app/resources/images/icon.png')
  __ANTIALIASING = False
  __GREEN = (0, 255, 0)
  __BLACK = (0, 0, 0)

  def __init__(self):
    self.__screen = pygame.display.get_surface()
    self.__window_icon()

  def background(self):
    self.__screen.fill(self.__BLACK)

  def text(self, text, size, position, has_update = False):
    font = self.__font(size)

    rendered = self.__render(text, font)
    self.__in(position, rendered)
    if has_update: pygame.display.update()

  def __render(self, text, font):
    rendered = font.render(
      text,
      self.__ANTIALIASING,
      self.__GREEN
    )

    return rendered

  def __in(self, position, rendered):
    text_box = rendered.get_rect()
    text_box.topright = position

    self.__screen.blit(rendered, text_box)

  def __font(self, size):
    return pygame.font.Font(self.__FONT_PATH, size)

  def __window_icon(self):
    icon = pygame.image.load(self.__ICON_PATH)
    pygame.display.set_icon(icon)
