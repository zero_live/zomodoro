
class Bus():
  def __init__(self):
    self.__subscriptions = {}

  def subscribe(self, topic, callback):
    if not self.__exists(topic):
      self.__subscriptions[topic] = []

    self.__subscriptions[topic].append(callback)

  def publish(self, topic, payload):
    if self.__exists(topic):
      for callback in self.__subscriptions[topic]:
        callback(payload)

  def __exists(self, topic):
    return topic in self.__subscriptions
