import pygame
import os

class Mixer():
  def play_power(self):
    self.__play('power.wav')

  def play_alarm(self):
    self.__play('alarm.wav')

  def __play(self, sound):
    sound_path = os.path.realpath('app/resources/sounds/' + sound)
    sound = pygame.mixer.Sound(sound_path)
    sound.set_volume(0.05)
    sound.play()
